/* 
 *  Copyright [2023] <qazxdrcssc2006@163.com>
 */

#include <memory>
#include <random>

#include <gtest/gtest.h>

#include "mock/gb/cartridge/MockGbCartridge.h"
#include "mock/gb/MockGbDevice.h"
#include "qnemu/gb/const.h"
#include "qnemu/gb/gpu/GbOam.h"
#include "qnemu/gb/gpu/GbVideoRam.h"

namespace qnemuTest
{

namespace {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, 255);
}

class GbOamTest : public testing::Test
{
public:
    void SetUp() override
    {
        oam = std::make_unique<qnemu::GbOam>(mockCartridge, mockVideoRam, mockWorkRam);
    }

// NOLINTBEGIN
protected:
    testing::StrictMock<qnemuMock::MockGbCartridge> mockCartridge;
    testing::StrictMock<qnemuMock::MockGbDevice> mockVideoRam;
    testing::StrictMock<qnemuMock::MockGbDevice> mockWorkRam;
    std::unique_ptr<qnemu::GbOam> oam;
// NOLINTEND
};

TEST_F(GbOamTest, ReadAndWriteData)
{
    for (uint32_t i = qnemu::OamStart; i <= qnemu::OamEnd; i++) {
        const uint8_t value = distrib(gen);
        oam->write(i, value);
        EXPECT_EQ(value, oam->read(i));
    }

    const uint8_t value = distrib(gen);
    oam->write(0xFF46, value);
    EXPECT_EQ(value, oam->read(0xFF46));
}

}  // namespace qnemuTest
