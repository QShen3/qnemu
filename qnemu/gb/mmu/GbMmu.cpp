/* 
 *  Copyright [2023] <qazxdrcssc2006@163.com>
 */

#include <cstdint>
#include <memory>

#include "qnemu/gb/cartridge/GbCartridge.h"
#include "qnemu/gb/const.h"
#include "qnemu/gb/gpu/GbGpuInterface.h"
#include "qnemu/gb/memory/GbWorkRam.h"
#include "qnemu/gb/mmu/GbMmu.h"

namespace qnemu
{

GbMmu::GbMmu(std::unique_ptr<GbApu> apu,
        std::shared_ptr<GbCartridgeInterface> cartridge,
        std::unique_ptr<GbGpuInterface> gpu,
        std::unique_ptr<GbHighRam> highRam,
        std::shared_ptr<GbInterruptHandlerInterface> interruptHandler,
        std::unique_ptr<GbJoypad> joypad,
        std::unique_ptr<GbWorkRam> workRam,
        std::unique_ptr<GbTimer> timer) :
    apu(std::move(apu)),
    cartridge(cartridge),
    gpu(std::move(gpu)),
    highRam(std::move(highRam)),
    interruptHandler(interruptHandler),
    joypad(std::move(joypad)),
    workRam(std::move(workRam)),
    timer(std::move(timer))
{

}

uint8_t GbMmu::read(uint16_t address) const
{
    if (address <= MemoryRomBank01End) {
        return cartridge->read(address);
    } else if (address <= VideoRamEnd) {
        return gpu->read(address);
    } else if (address <= ExternalRamEnd) {
        return cartridge->read(address);
    } else if (address <= EchoRamEnd) {
        return workRam->read(address);
    } else if (address <= OamEnd) {
        return gpu->read(address);
    } else if (address == 0xFF00) {
        return joypad->read(address);
    } else if (address >= 0xFF04 && address <= 0xFF07) {
        return timer->read(address);
    } else if (address >= 0xFF10 && address <= 0xFF26) {
        return apu->read(address);
    } else if (address >= 0xFF30 && address <= 0xFF3F) {
        return apu->read(address);
    } else if (address >= 0xFF40 && address <= 0xFF4B) {
        return gpu->read(address);
    } else if (address == 0xFF4F) {
        return gpu->read(address);
    } else if (address >= 0xFF51 && address <= 0xFF55) {
        return gpu->read(address);
    } else if (address >= 0xFF68 && address <= 0xFF6B) {
        return gpu->read(address);
    } else if (address == 0xFF70) {
        return workRam->read(address);
    } else if (address >= HighRamStart && address <= HighRamEnd) {
        return highRam->read(address);
    } else if (address == 0xFFFF) {
        return interruptHandler->read(address);
    } else {
        return 0xFF;
    }
}

void GbMmu::write(uint16_t address, const uint8_t& value)
{
    if (address <= MemoryRomBank01End) {
        cartridge->write(address, value);
    } else if (address <= VideoRamEnd) {
        gpu->write(address, value);
    } else if (address <= ExternalRamEnd) {
        cartridge->write(address, value);
    } else if (address <= EchoRamEnd) {
        workRam->write(address, value);
    } else if (address <= OamEnd) {
        gpu->write(address, value);
    } else if (address == 0xFF00) {
        joypad->write(address, value);
    } else if (address >= 0xFF04 && address <= 0xFF07) {
        timer->write(address, value);
    } else if (address >= 0xFF10 && address <= 0xFF26) {
        apu->write(address, value);
    } else if (address >= 0xFF30 && address <= 0xFF3F) {
        apu->write(address, value);
    } else if (address >= 0xFF40 && address <= 0xFF4B) {
        gpu->write(address, value);
    } else if (address == 0xFF4F) {
        gpu->write(address, value);
    } else if (address >= 0xFF51 && address <= 0xFF55) {
        gpu->write(address, value);
    } else if (address >= 0xFF68 && address <= 0xFF6B) {
        gpu->write(address, value);
    } else if (address == 0xFF70) {
        workRam->write(address, value);
    } else if (address >= HighRamStart && address <= HighRamEnd) {
        highRam->write(address, value);
    } else if (address == 0xFFFF) {
        interruptHandler->write(address, value);
    } else {
        return;
    }
}

void GbMmu::step()
{
    apu->step();
    cartridge->step();
    gpu->step();
    workRam->step();
    highRam->step();
    timer->step();
    joypad->step();
    interruptHandler->step();
}

void GbMmu::reset()
{
    apu->reset();
    cartridge->reset();
    gpu->reset();
    workRam->reset();
    highRam->reset();
    timer->reset();
    joypad->reset();
    interruptHandler->reset();
}

}  // namespace qnemu